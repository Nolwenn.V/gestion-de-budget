Recherche et analyse des besoins

QUI ?
tout ceux qui veulent un outil de gestion de budget.

QUEL USAGE ?
personel

QUOI ?
obligatoire:
gérer un budget mensuel : entrées et sorties
les entrées doivent pouvoir être catégorisées (loisirs, famille, loyer/prêt immobilier, etc)
il doit être possible de "geler" une entrée : une entrée gelée n'est plus modifiable
l'application doit permettre d'éditer des statistiques mensuelles sur le pourcentage de dépenses par type, l'évolution au fil du temps, etc

optionel:
gérer un budget commun, et des budgets séparés
pour les parents de voir le budget de leurs enfants, mais pas l'inverse
pouvoir faire un budget prévisionnel, c'est à dire dans le futur, pour planifier des grosses dépenses (vacances, réparation de voiture, achat immobilier, etc)
voir son solde bancaire et son évolution au fil du temps
ajouter des justificatifs (tickets de caisse, ticket de péage, facture, etc)
désigner une entrée comme étant récurrente (elle est automatiquement ajoutée tous les mois, par exemple votre abonnement à Internet)

COMMENT ?
contexte final:
une appli web
techno: 
Java/MySQL
delai:
??
